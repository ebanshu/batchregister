e板书批量注册文档
- 本脚本功能为批量注册e板书（2.0和3.0系统）用户，包括学生和老师账号: user_rest.py
- 批量为学生添加至课程(e板书3.0系统）: add_course.py 

注意：
- 本脚本执行需要装有python2.7环境
- 安装依赖包：pip install -r requirements.txt
- 执行前修改settings.py文件：
    -- PUB_USER_API_HOST指定为要注册用户的站点域名或IP
    -- CORE_CLASSROOM_API_HOST为批量添加学生至课程时需要的ebs3.0的core api的HOST

使用方法
1. 单个excel文件批量注册
    1) 将excel文件命名为'userlist.xlsx'
    2) excel文件内容第2行开始为正文，第1列为用户名，第2列为姓名
    3) 若在2.0系统注册:
        3.1) 在user_rest.py文件的第109行修改为：
            req = Ebs2UserRegister()
        3.2) 若注册教师账号，修改user_rest.py的第146行第后一个参数is_teacher=True
        3.3) 在命令行执行：python user_rest.py 
    4) 若在3.0系统注册：
        4.1) 在user_rest.py文件的第109行修改为：
            req = UserRegister()
        4.2) 若注册教师账号，修改user_rest.py的第146行第后一个参数is_teacher=True
        4.3) 在命令行执行：python user_rest.py 

2. 多个excel文件批量注册 
    1) 将所有excel文件放在当前目录下的文件中, 如hbsd
    2) 每个excel的文件内容格式需要跟第1点的第2)小点一致
    3) 修改user_rest.py文件163行为(函数参数为文件夹名称)：
        batch_register_with_folder('hbsd')
    4) 在命令行执行：python user_rest.py 

3. 批量添加学生至课程中 
    1) 在第1或第2步执行的批量注册中，会生成批量注册完成后所有的用户id，并保存在当前目录的result文件中，该文件里面的用户id也即为添加至课程的用户id
    2) 打开add_course.py文件，第25行为需要添加至的所有课程id, 根据实际情况修改
    3) 在命令行执行: python add_course.py


