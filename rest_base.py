#coding=utf-8


import urllib
import urllib2
import logging
import simplejson

import settings

def encode_get_args(args):
    def _to_quoted_utf8(s):
        if not isinstance(s, basestring):
            s = str(s)
        if isinstance(s, unicode):
            s = s.encode('utf-8')
        return urllib.quote(s)
    return '&'.join([ '='.join([item[0], _to_quoted_utf8(item[1])]) for item in args.items()])

def encode_post_args(args):
    return simplejson.dumps(args)

def decode_response(body):
    data = simplejson.loads(body)
    return data

class RestRequest(object):

    def __init__(self, host, method=None, protocol_prefix='http://', **kwargs):
        self.__dict__['_properties'] = {}
        self._host = protocol_prefix + host
        self._method = method
        self._response = None
        self._curry = {}
        self._kwargs = kwargs

    def __setattr__(self, attr, value):
        if attr not in ['_properties', '_host', '_method', '_response', '_curry', '_user_id', '_client', '_kwargs']:
            if attr in self._curry:
                self._properties[attr] = self._curry[attr][1](value)
            else:
                self._properties[attr] = value
        else:
            self.__dict__[attr] = value

    def __getattr__(self, attr):
        if attr not in ['_properties', '_host', '_method', '_response', '_curry', '_user_id', '_client', '_kwargs']:
            value = self._properties.get(attr, None)
            if attr in self._curry:
                return self._curry[attr][0](value)
            else:
                return value
        else:
            return self.__dict__.get(attr, None)

    def get_url(self):
        return self._host

    def get_response(self):
        return self._response

    def fetch(self):
        raise NotImplemented

    def get_validated_args(self):
        return self._properties
    
    def _format_response(self, response, format='json'):

        try:
            body = response.read()
            if settings.ECHO:
                print 'response :', body
            if format == 'json':
                self._response = decode_response(body)
            elif format == 'raw':
                self._response = body

        except Exception, e:
            logging.error('rest %s request exception, exception: %s'%(self.get_url(), e))

    def data(self):
        if self._response:
            return self._response['data']

    def ok(self):
        if self._response:
            return self._response['ok']

    def reason(self):
        if self._response:
            reason = self._response['reason']
            if reason:
                try:
                    return reason
                except:
                    logging.error('rest %s get invalid error code, response: %s'%(self.get_url(), self._response))
                    return ''
            return ''

    def reason_code(self):
        return self._response.get('reason', 0)


class RestGetRequest(RestRequest):

    def __init__(self, host, **kwargs):
        RestRequest.__init__(self, host, method='GET', **kwargs)

    def fetch(self, url=None, format='json', timeout=settings.APP_REQUEST_TIMEOUT):
        if not url:
            url = self.get_url()
        if settings.ECHO:
            print "request url ", self._method, ": ", url, self.get_validated_args()
        body = encode_get_args(self.get_validated_args())
        if body:
            url += '?%s' % body
        try:
            response = urllib2.urlopen(url, timeout=timeout)
            self._format_response(response, format=format)
        except:
            self._response = {'ok': False, 'data': None, 'reason': 'url cannot be fetched'}


class RestPostRequest(RestRequest):

    def __init__(self, host, **kwargs):
        RestRequest.__init__(self, host, method='POST', **kwargs)

    def fetch(self, url=None, format='json', timeout=settings.APP_REQUEST_TIMEOUT):
        if not url:
            url = self.get_url()
        if settings.ECHO:
            print "request url ", self._method, ": ", url, self.get_validated_args()
        body = encode_post_args(self.get_validated_args())
        try:
            response = urllib2.urlopen(url, data=body, timeout=timeout)
            self._format_response(response, format=format)
        except:
            self._response = {'ok': False, 'data': None, 'reason': 'url cannot be fetched'}






