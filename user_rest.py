import settings
import json
import xlrd

from os import listdir
from os.path import isfile, join
from file_utils import json_file
from rest_base import RestGetRequest, RestPostRequest

class QueryClassroomSectionList(RestGetRequest):
    """
    Rest Client:  Query classroom section list
    """
    def __init__(self, host=settings.PUB_USER_API_HOST):
        RestGetRequest.__init__(self, host)

    def get_url(self):
        return '%s/api/classroom/section/list/' % self._host

def query_section_list(classroom_id):
    req = QueryClassroomSectionList()
    req.classroom_id = classroom_id
    req.fetch()
    if req.ok():
        return [s['id'] for s in req.data()]
    return []


class QueryFrameList(RestGetRequest):
    """
    Rest Client:  Query frame list
    """
    def __init__(self, host=settings.PUB_USER_API_HOST):
        RestGetRequest.__init__(self, host)

    def get_url(self):
        return '%s/api/classroom/frame/list/' % self._host

def core_get_frame_list_api(section_id):
    """
    Core API: get the frame list in a section
    """
    req = QueryFrameList()
    req.section_id = section_id
    req.fetch()
    if req.ok():
        return req.data()
    return None

class QueryCanvasDraws(RestGetRequest):
    """
    Rest Client:  Query frame list
    """
    def __init__(self, host=settings.PUB_USER_API_HOST):
        RestGetRequest.__init__(self, host)
        self._frame_id = '0'

    def get_url(self):
        return '%s/node/api/draws/%s/' % (self._host, self._frame_id)

def core_get_canvas_draws_api(frame_id):
    req = QueryCanvasDraws()
    req._frame_id = str(frame_id)
    req.fetch()
    return req.get_response()


class QueryCanvasChatMessage(RestGetRequest):
    """ 
    Rest Client:  Query frame list
    """
    def __init__(self, host=settings.PUB_USER_API_HOST):
        RestGetRequest.__init__(self, host)
        self._frame_id = '0' 

    def get_url(self):
        return '%s/node/api/messages/%s/%d/' % (self._host, self.classroom_id, 20000000)

def core_get_canvas_chat_message_api(classroom_id):
    req = QueryCanvasChatMessage()
    req.classroom_id = str(classroom_id)
    req.fetch()
    return req.get_response()

class UserRegister(RestGetRequest):
    """
    Rest Client:  register
    """
    def __init__(self, host=settings.PUB_USER_API_HOST):
        RestGetRequest.__init__(self, host)

    def get_url(self):
        return '%s/public/api/user/script/register/' % self._host

class Ebs2UserRegister(RestGetRequest):
    """
    Rest Client:  register
    """
    def __init__(self, host=settings.PUB_USER_API_HOST):
        RestGetRequest.__init__(self, host)

    def get_url(self):
        return '%s/api/user/register/' % self._host


def core_user_register_api(username, email, password, nick=None, is_teacher=None):
    """
    """
    #req = UserRegister()
    req = Ebs2UserRegister()
    req.username = username
    req.email = email
    req.password = password
    if is_teacher:
        req.is_teacher = True
    if nick:
        req.nick = nick
    req.fetch()
    if req.ok():
        return req.data()
    return None
 
def writeIdsTofile(ids):
    f = open('result', 'w')
    for i in ids:
        f.write(str(i)+"\n")
    f.close()

def batch_register_users(file_name=None):
    if not file_name:
        file_name = 'userlist.xlsx'
    print 'start to register......'
    print 'file: ', file_name
    xlsfile = xlrd.open_workbook(file_name)
    sheet = xlsfile.sheet_by_name('Sheet1')
    print sheet.nrows, sheet.ncols
    registed_userids = []
    for i in range(1, sheet.nrows):
        username = str(sheet.cell(i, 0).value)
        nick = sheet.cell(i, 1).value
        password = '123456'
        if len(username) >= 6:
            password = username[len(username)-6:]
        email = username + '@ebanshu.com'
        print 'username: ', username
        data = core_user_register_api(username, email, password, nick=nick, is_teacher=False)
        print data
        if data and data.get('id', None):
            registed_userids.append(data['id']) 
    writeIdsTofile(registed_userids)
    print 'finished.'

def batch_register_with_folder(directory_name=None):
    if not directory_name:
        batch_register_users()
    directory = './%s' % directory_name
    onlyfiles = [f for f in listdir(directory) if isfile(join(directory, f)) and f.find('.xlsx') > 0]
    for file_name in onlyfiles:
        file_name = '%s/%s' % (directory, file_name)
        batch_register_users(file_name)

if __name__ == '__main__':
    batch_register_users()
    #batch_register_with_folder('hbsd')

