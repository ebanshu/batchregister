from rest_base import RestGetRequest, RestPostRequest
import settings
import json
import xlrd
from file_utils import json_file

class AddUserLearning(RestGetRequest):

    def __init__(self, host=settings.CORE_CLASSROOM_API_HOST):
        RestGetRequest.__init__(self, host)

    def get_url(self):
        return '%s/core/api/course/insert/user/learning/' % self._host


def core_add_user_learning(user_id, course_id):
    req = AddUserLearning()
    req.course_id = course_id
    req.user_id = user_id
    req.fetch()
    if req.ok():
        return req.data()
    return None

course_ids = [1000346, 1000347, 1000348, 1000349, 1000350, 1000351, 1000352]

def batch_add_courses():
    f = open('result', 'r')
    while 1:
        line = f.readline()
        if not line:
            break;
        print 'user_id: ', line
        for course_id in course_ids:
            data = core_add_user_learning(line, course_id)
            print 'added: ', data

if __name__ == '__main__':
    batch_add_courses()




